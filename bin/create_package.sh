#!/usr/bin/env bash

set -e

PROJECT_PATH=$(dirname $(cd $(dirname $0) && pwd))
PROJECT_NAME=$(basename ${PROJECT_PATH})
CONSOLE="php bin/console"
CUR_TIMESTAMP=$(date +"%F_%H%M%S")
OUTPUT_DIR=$1
TMP_DIR="/tmp/${PROJECT_NAME}/${CUR_TIMESTAMP}"

if [[ -z "${OUTPUT_DIR}" ]]; then
    OUTPUT_DIR="${PROJECT_PATH}/build"
fi

exec_cmd () {
    echo Exec : $@
    sh -c "$@"
}

exec_cmd "mkdir -p ${TMP_DIR}"
cd ${TMP_DIR}
exec_cmd "git clone -b master --single-branch ${PROJECT_PATH} ."
exec_cmd "composer install --no-dev"
exec_cmd "mkdir -p ${OUTPUT_DIR}"
exec_cmd "tar -czf ${OUTPUT_DIR}/${PROJECT_NAME}_${CUR_TIMESTAMP}.tar ."
