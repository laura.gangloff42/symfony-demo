#!/usr/bin/env bash

set -e

PROJECT_PATH=$(dirname $(cd $(dirname $0) && pwd))
CONSOLE="php bin/console"

exec_cmd () {
    echo Exec : $@
    sh -c "$@"
}

RUN_UNIT=false
RUN_INTEGRATION=false
RUN_FUNCTIONAL=false
RUN_ACCEPTANCE=false

usage() {
    echo "Usage: $0 [-u] [-n] [-r] [-d]" 1>&2;
    echo "Steps (if specified):" 1>&2;
    echo " [ Run all if omitted ]" 1>&2;
    echo "  -u Run unit test" 1>&2;
    echo "  -i Run integration test" 1>&2;
    echo "  -f Run functional test" 1>&2;
    echo "  -a Run acceptance test" 1>&2;
    exit 1;
}

all_steps() {
    RUN_ALL=true
}

if [[ "${#}" -eq 0 ]]
then
    all_steps
fi

while getopts ":uifa" o; do
    case "${o}" in
        u)
            RUN_UNIT=true
            ;;
        i)
            RUN_INTEGRATION=true
            ;;
        f)
            RUN_FUNCTIONAL=true
            ;;
        a)
            RUN_ACCEPTANCE=true
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))



main() {
    if [[ "${RUN_UNIT}" = "true" ]]
    then
        echo "Run unit test..."
        exec_cmd "php vendor/bin/codecept run unit"
    fi

    if [[ "${RUN_INTEGRATION}" = "true" ]]
    then
        echo "Run integration..."
        exec_cmd "php vendor/bin/codecept run integration"
    fi

    if [[ "${RUN_FUNCTIONAL}" = "true" ]]
    then
        echo "Run functional test..."
        exec_cmd "php vendor/bin/codecept run functional"
    fi

    if [[ "${RUN_ACCEPTANCE}" = "true" ]]
    then
        echo "Run acceptance test..."
        exec_cmd "php vendor/bin/codecept run acceptance"
    fi

    if [[ "${RUN_ALL}" = "true" ]]
    then
        echo "Run all test..."
        exec_cmd "php vendor/bin/codecept run"
    fi

}

main
