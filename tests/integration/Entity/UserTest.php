<?php


namespace App\Tests\Entity;

use App\Entity\User;
use Codeception\Test\Unit;

class UserTest extends Unit
{

    public function testAddUser()
    {
        $user = new User();
        $user->setFullName('test');
        $user->setUsername('test');
        $user->setEmail('test@test.com');
        $user->setRoles(['ROLE_USER']);
        $user->setPassword("test");


        $em = $this->getModule('Doctrine2')->em;
        $em->persist($user);
        $em->flush();
        $this->assertEquals('test', $user->getUsername());
        // verify data was saved using framework methods
        $this->tester->seeInRepository(User::class, ['username' => 'test']);
        $this->tester->dontSeeInRepository(User::class, ['username' => 'test2']);
    }
}
