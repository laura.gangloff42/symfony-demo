<?php
use App\Tests\AcceptanceTester;
$I = new AcceptanceTester($scenario);
$I->wantTo('See welcome message');
$I->amOnPage('/en');
$I->see("Welcome to the Symfony Demo application");
